<?php
require_once('app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

error_reporting(E_ALL|E_STRICT); ini_set('display_errors', 1);

$faqHelper = Mage::helper('faq');

$faqCategories = $faqHelper->getFaqCategories();

foreach ($faqCategories as $faqCategory)
{
	echo '<div>' . $faqCategory->getTitle() . '</div>';
	echo '<div>' . $faqCategory->getDescription() . '</div>';
	echo '<div>' . $faqCategory->getName() . '</div>';
	
	
	
	foreach ($faqHelper->getFaqQuestionsByCategoryName($faqCategory->getName()) as $question)
	{
		echo '<div>' . $question->getTitle() . '</div>';
		echo '<div>' . $question->getDescription() . '</div>';
	}

}
