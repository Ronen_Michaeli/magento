<?php

class Robin_Faq_Block_Adminhtml_Categories_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("categoriesGrid");
				$this->setDefaultSort("category_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("faq/categories")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("category_id", array(
				"header" => Mage::helper("faq")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "category_id",
				));
                
				$this->addColumn("name", array(
				"header" => Mage::helper("faq")->__("Name"),
				"index" => "name",
				));
				$this->addColumn("title", array(
				"header" => Mage::helper("faq")->__("Title"),
				"index" => "title",
				));
						$this->addColumn('status', array(
						'header' => Mage::helper('faq')->__('Status'),
						'index' => 'status',
						'type' => 'options',
						'options'=>Robin_Faq_Block_Adminhtml_Categories_Grid::getOptionArray4(),				
						));
						
					$this->addColumn('created_at', array(
						'header'    => Mage::helper('faq')->__('Created at'),
						'index'     => 'created_at',
						'type'      => 'datetime',
					));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('category_id');
			$this->getMassactionBlock()->setFormFieldName('category_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_categories', array(
					 'label'=> Mage::helper('faq')->__('Remove Categories'),
					 'url'  => $this->getUrl('*/adminhtml_categories/massRemove'),
					 'confirm' => Mage::helper('faq')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray4()
		{
            $data_array=array(); 
			$data_array[0]= 'disable';
			$data_array[1]= 'enable';
            return($data_array);
		}
		static public function getValueArray4()
		{
            $data_array=array();
			foreach(Robin_Faq_Block_Adminhtml_Categories_Grid::getOptionArray4() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}