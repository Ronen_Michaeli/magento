<?php

class Robin_Faq_Block_Adminhtml_Questions_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("questionsGrid");
				$this->setDefaultSort("question_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("faq/questions")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("question_id", array(
				"header" => Mage::helper("faq")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "question_id",
				));
                
				$this->addColumn("name", array(
				"header" => Mage::helper("faq")->__("Name"),
				"index" => "name",
				));
				$this->addColumn("title", array(
				"header" => Mage::helper("faq")->__("Title"),
				"index" => "title",
				));
						$this->addColumn('status', array(
						'header' => Mage::helper('faq')->__('Status'),
						'index' => 'status',
						'type' => 'options',
						'options'=>Robin_Faq_Block_Adminhtml_Questions_Grid::getOptionArray9(),				
						));
						
				$this->addColumn("category", array(
				"header" => Mage::helper("faq")->__("Category"),
				"index" => "category",
				));
					$this->addColumn('created_at', array(
						'header'    => Mage::helper('faq')->__('Created at'),
						'index'     => 'created_at',
						'type'      => 'datetime',
					));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('question_id');
			$this->getMassactionBlock()->setFormFieldName('question_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_questions', array(
					 'label'=> Mage::helper('faq')->__('Remove Questions'),
					 'url'  => $this->getUrl('*/adminhtml_questions/massRemove'),
					 'confirm' => Mage::helper('faq')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray9()
		{
            $data_array=array(); 
			$data_array[0]='disable';
			$data_array[1]='enable';
            return($data_array);
		}
		static public function getValueArray9()
		{
            $data_array=array();
			foreach(Robin_Faq_Block_Adminhtml_Questions_Grid::getOptionArray9() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}