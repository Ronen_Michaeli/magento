<?php


class Robin_Faq_Block_Adminhtml_Categories extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_categories";
	$this->_blockGroup = "faq";
	$this->_headerText = Mage::helper("faq")->__("Categories Manager");
	$this->_addButtonLabel = Mage::helper("faq")->__("Add New Item");
	parent::__construct();
	
	}

}