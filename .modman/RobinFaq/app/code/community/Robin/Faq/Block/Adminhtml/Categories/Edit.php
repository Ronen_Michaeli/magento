<?php
	
class Robin_Faq_Block_Adminhtml_Categories_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "category_id";
				$this->_blockGroup = "faq";
				$this->_controller = "adminhtml_categories";
				$this->_updateButton("save", "label", Mage::helper("faq")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("faq")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("faq")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("categories_data") && Mage::registry("categories_data")->getId() ){

				    return Mage::helper("faq")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("categories_data")->getId()));

				} 
				else{

				     return Mage::helper("faq")->__("Add Item");

				}
		}
}