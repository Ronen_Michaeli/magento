<?php
class Robin_Faq_Block_Adminhtml_Categories_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("faq_form", array("legend"=>Mage::helper("faq")->__("Item information")));

				
						$fieldset->addField("name", "text", array(
						"label" => Mage::helper("faq")->__("Name"),
						"name" => "name",
						));
					
						$fieldset->addField("title", "text", array(
						"label" => Mage::helper("faq")->__("Title"),
						"name" => "title",
						));
					
						$fieldset->addField("description", "textarea", array(
						"label" => Mage::helper("faq")->__("Description"),
						"name" => "description",
						));
									
						 $fieldset->addField('status', 'select', array(
						'label'     => Mage::helper('faq')->__('Status'),
						'values'   => Robin_Faq_Block_Adminhtml_Categories_Grid::getValueArray4(),
						'name' => 'status',
						));
						$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
							Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
						);

						$fieldset->addField('created_at', 'date', array(
						'label'        => Mage::helper('faq')->__('Created at'),
						'name'         => 'created_at',
						'time' => true,
						'image'        => $this->getSkinUrl('images/grid-cal.gif'),
						'format'       => $dateFormatIso
						));

				if (Mage::getSingleton("adminhtml/session")->getCategoriesData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getCategoriesData());
					Mage::getSingleton("adminhtml/session")->setCategoriesData(null);
				} 
				elseif(Mage::registry("categories_data")) {
				    $form->setValues(Mage::registry("categories_data")->getData());
				}
				return parent::_prepareForm();
		}
}
