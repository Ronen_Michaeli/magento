<?php


class Robin_Faq_Block_Adminhtml_Questions extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_questions";
	$this->_blockGroup = "faq";
	$this->_headerText = Mage::helper("faq")->__("Questions Manager");
	$this->_addButtonLabel = Mage::helper("faq")->__("Add New Item");
	parent::__construct();
	
	}

}