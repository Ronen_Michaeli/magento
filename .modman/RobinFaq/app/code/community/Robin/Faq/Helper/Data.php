<?php
class Robin_Faq_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getFaqCategories()
	{
		return Mage::getModel('faq/categories')->getCollection()
										->addFieldToFilter('status', 1);
	}
	
	public function getFaqQuestionsByCategoryName($categoryName)  
	{
		return Mage::getModel('faq/questions')->getCollection()
										->addFieldToFilter('status', 1)
										->addFieldToFilter('category', $categoryName);
	}
	
}
	 