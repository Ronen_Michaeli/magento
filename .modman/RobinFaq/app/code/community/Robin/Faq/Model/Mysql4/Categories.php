<?php
class Robin_Faq_Model_Mysql4_Categories extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("faq/categories", "category_id");
    }
}