<?php
class Robin_Faq_Model_Mysql4_Questions extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("faq/questions", "question_id");
    }
}